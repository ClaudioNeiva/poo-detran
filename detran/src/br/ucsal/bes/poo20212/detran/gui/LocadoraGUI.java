package br.ucsal.bes.poo20212.detran.gui;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

// Obs: para a implementação da interface gráfica foi utilizado o plugin Window Builder para eclipse.
public class LocadoraGUI {

	private JFrame frame;

	public static void inicializar() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LocadoraGUI window = new LocadoraGUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public LocadoraGUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setTitle("Locadora");
		frame.setBounds(100, 100, 450, 350);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JMenuBar menuBar = new JMenuBar();
		frame.setJMenuBar(menuBar);

		JMenu mnVeculo = new JMenu("Veículo");
		menuBar.add(mnVeculo);

		JMenuItem mntmCadastrar = new JMenuItem("Cadastrar");
		mntmCadastrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				atualizarPainel(new VeiculoCadastroGUI());
			}
		});
		mnVeculo.add(mntmCadastrar);

		JMenuItem mntmListar = new JMenuItem("Listar");
		mntmListar.addActionListener(e -> atualizarPainel(new VeiculoListagemGUI()));
		mnVeculo.add(mntmListar);
	}

	private void atualizarPainel(JPanel painel) {
		frame.getContentPane().removeAll();
		frame.setContentPane(painel);
		frame.revalidate();
	}
}
