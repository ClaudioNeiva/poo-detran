package br.ucsal.bes.poo20212.detran.util;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import br.ucsal.bes.poo20212.detran.exception.NegocioException;

public class FileUtil {

	private FileUtil() {
	}

	public static void gravar(String nomeArquivo, Object objeto) throws NegocioException {
		try (FileOutputStream fos = new FileOutputStream(nomeArquivo);
				ObjectOutputStream oos = new ObjectOutputStream(fos);) {
			oos.writeObject(objeto);
		} catch (IOException e) {
			throw new NegocioException(e.getMessage(), e);
		}
	}

	public static Object ler(String nomeArquivo) throws IOException, ClassNotFoundException {
		try (FileInputStream fis = new FileInputStream(nomeArquivo);
				ObjectInputStream ois = new ObjectInputStream(fis);) {
			return ois.readObject();
		} 
	}

}
