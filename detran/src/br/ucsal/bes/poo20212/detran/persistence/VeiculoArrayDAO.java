package br.ucsal.bes.poo20212.detran.persistence;

import br.ucsal.bes.poo20212.detran.domain.Veiculo;

public class VeiculoArrayDAO {

	private static final int QTD_MAX_VEICULOS = 1000;

	private static Veiculo[] veiculos = new Veiculo[QTD_MAX_VEICULOS];
	private static int qtd = 0;

	public static void insert(Veiculo veiculo) {
		if (qtd <= QTD_MAX_VEICULOS) {
			veiculos[qtd] = veiculo;
			qtd++;
		}
	}

	public static void remove(int pos) {
		if (pos >= 0 && pos < qtd) {
			for (int i = pos; i < qtd - 1; i++) {
				veiculos[i] = veiculos[i + 1];
			}
			veiculos[qtd] = null;
			qtd--;
		}
	}

	public static Veiculo findByIndex(int pos) {
		if (pos >= 0 && pos < qtd) {
			return veiculos[pos];
		}
		return null;
	}

	public static Veiculo findByPlaca(String placa) {
		for (int i = 0; i < qtd; i++) {
			Veiculo veiculo = veiculos[i];
			if (veiculo.getPlaca().equals(placa)) {
				return veiculo;
			}
		}
		return null;
	}

	public static Veiculo findByPlaca2(String placa) {
		for (Veiculo veiculo : veiculos) {
			if (veiculo != null && veiculo.getPlaca().equals(placa)) {
				return veiculo;
			}
		}
		return null;
	}

	public static Veiculo[] findAll() {
		Veiculo[] veiculosCadastrados = new Veiculo[qtd];
		for (int i = 0; i < qtd; i++) {
			veiculosCadastrados[i] = veiculos[i];
		}
		return veiculosCadastrados;
		// return Arrays.copyOf(veiculos, qtd);
	}

}


