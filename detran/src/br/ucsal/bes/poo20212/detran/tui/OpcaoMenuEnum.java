package br.ucsal.bes.poo20212.detran.tui;

public enum OpcaoMenuEnum {

	INCLUIR_PROPRIETARIO(1, "Incluir proprietário"),

	INCLUIR_VEICULO(2, "Incluir veículo"),

	ALTERAR_VEICULO(3, "Alterar veículo"),

	LISTAR_VEICULOS(4, "Listar veículos"),

	CALCULAR_IMPOSTO(5, "Calcular imposto"),

	SAIR(9, "Sair");

	private Integer codigo;

	private String descricao;

	private OpcaoMenuEnum(Integer codigo, String descricao) {
		this.codigo = codigo;
		this.descricao = descricao;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public String getDescricao() {
		return descricao;
	}

	public String obterDescricaoCompleta() {
		return codigo + " - " + descricao;
	}

	public static OpcaoMenuEnum valueOfInteger(Integer opcaoInteger) {
		for (OpcaoMenuEnum opcaoMenu : values()) {
			if (opcaoMenu.codigo.equals(opcaoInteger)) {
				return opcaoMenu;
			}
		}
		return null;
	}

}
