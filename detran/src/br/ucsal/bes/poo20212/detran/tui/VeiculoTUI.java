package br.ucsal.bes.poo20212.detran.tui;

import java.util.List;

import br.ucsal.bes.poo20212.detran.business.VeiculoBO;
import br.ucsal.bes.poo20212.detran.domain.Veiculo;
import br.ucsal.bes.poo20212.detran.exception.NegocioException;
import br.ucsal.bes.poo20212.detran.persistence.VeiculoDAO;
import br.ucsal.bes.poo20212.detran.tui.util.UtilTUI;

public class VeiculoTUI {

	private static final Integer SIM = 1;

	public static void incluir() {
		System.out.println("############ INCLUSÃO DE VEÍCULOS ############");

		Integer resposta = SIM;

		do {

			String placa = UtilTUI.obterString("Informe a placa:");
			int anoFabricacao = UtilTUI.obterInteger("Informe o ano de fabricação:");
			double valor = UtilTUI.obterDouble("Informe o valor:");

			Veiculo veiculo = new Veiculo(placa, anoFabricacao, valor);

			try {
				VeiculoBO.incluir(veiculo);
				System.out.println("Inclusão feita com sucesso.");
				return;
			} catch (NegocioException e) {
				System.out.println(e.getMessage());
				resposta = UtilTUI.obterInteger("Deseja tentar incluir de novo, corrigindo os dados (1 para sim e 2 para não)? ");
			} finally {
				System.out.println("O que vc colocar no finally vai ocorrer incondicionalmente!");
			}

			// System.out.println("outras coisas vão continuar ocorrendo normalmente");

		} while (resposta == SIM);

	}

	public static void alterar() {
		System.out.println("############ ALTERAÇÃO DE VEÍCULOS ############");

		String placaPesquisa = UtilTUI.obterString("Informe a placa do veículo a ser alterado:");

		Veiculo veiculo = VeiculoDAO.findByPlaca(placaPesquisa);

		if (veiculo == null) {
			System.out.println("Veículo não encontrado.");
			return;
		}

		String placa = UtilTUI
				.obterString("Informe a nova placa (vazio para não alterar) [" + veiculo.getPlaca() + "]:");
		int anoFabricacao = UtilTUI.obterInteger(
				"Informe o novo ano de fabricação (0 para não alterar) [" + veiculo.getAnoFabricacao() + "]:");
		double valor = UtilTUI.obterDouble("Informe o novo valor (0 para não alterar) [" + veiculo.getValor() + "]:");

		try {
			VeiculoBO.alterar(veiculo, placa, anoFabricacao, valor);
		} catch (NegocioException e) {
			System.out.println(e.getMessage());
		}
	}

	public static void listar() {
		System.out.println("############ LISTAGEM DE VEÍCULOS ############");
		List<Veiculo> veiculos = VeiculoDAO.findAll();
		for (Veiculo veiculo : veiculos) {
			System.out.println("\nPlaca =" + veiculo.getPlaca());
			System.out.println("Ano de fabricação=" + veiculo.getAnoFabricacao());
			System.out.println("Valor=" + veiculo.getValor());
		}
	}

	public static void calcularImposto() {
		System.out.println("############ CALCULAR IMPOSTO ############");
		String placaSelecionada = UtilTUI.obterString("Informe a placa do veículo para cálculo do imposto:");
		Double valorImposto;
		try {
			valorImposto = VeiculoBO.calcularImposto(placaSelecionada);
			System.out.println("Imposto = " + valorImposto);
		} catch (NegocioException e) {
			System.out.println(e.getMessage());
		}
	}

}
